
class CfgPatches
{
    class AGC
    {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.0;
        requiredAddons[] = {
            "cba_main"
        };
        version = 1.0;
        versionStr = 1.0;
        versionAr[] = {1,0};
        author = "Madin";
    };
};

#include "CfgEden.hpp"
#include "CfgFunctions.hpp"
#include "XEH.hpp"

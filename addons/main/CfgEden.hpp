
class Cfg3DEN
{
    class Object
    {
        class AttributeCategories
        {
            class AGC_Attributes
            {
                displayName = "Advanced Garbage Collector";
                collapsed  = 0;

                class Attributes
                {
                    class AGC_blacklist
                    {
                        //--- Mandatory properties
                        displayName = $STR_AGC_excludeGC;
                        tooltip = $STR_AGC_excludeGCTip;
                        property = "AGC_gc_exclude";
                        control = "Checkbox";
                        expression = "_this setVariable ['GCblackList', _value, true];";
                        defaultValue = "false";
                        condition = "objectBrain"; // https://community.bistudio.com/wiki/Eden_Editor:_Configuring_Attributes#Condition
                        typeName = "BOOL";
                    };
                    class AGC_removeItems
                    {
                        //--- Mandatory properties
                        displayName = $STR_AGC_excludeItemRemove;
                        tooltip = $STR_AGC_excludeItemRemoveTip;
                        property = "AGC_item_exclude";
                        control = "Checkbox";
                        expression = "_this setVariable ['AGC_removeItems', _value, false];";
                        defaultValue = "false";
                        condition = "objectBrain"; // https://community.bistudio.com/wiki/Eden_Editor:_Configuring_Attributes#Condition
                        typeName = "BOOL";
                    };
                };
            };
        };
    };
};
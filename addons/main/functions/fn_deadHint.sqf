 /*
	AGC_fnc_deadHint

	Description:
		Check if player can check body count, if true then send command to the server.

	Arguments:
		0: show info to all players <BOOL>

	Return Value:
		Successful <BOOL>
 */

params [["_showToAll",false]];
if (isNull (getAssignedCuratorLogic player)) then {_showToAll = false};
if (!(isNull (getAssignedCuratorLogic player)) || AGC_showBodyCount) then
{
	[_showToAll] remoteExecCall ["AGC_fnc_deadHintServer",2,false];
}else
{
	systemChat localize "STR_AGC_CBA_showBodyError";
};
true

 /*
	AGC_fnc_removeItems

	Description:
		Remove items and magazines from dead AI. In body stays only % of items set in CBA settings.
		set 100% to disable script.

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		Successful <BOOL>
 */
 
if (AGC_removeItems == 100) exitWith {};
private _unit = _this select 0;
[
	{
		{if ((random 100) > AGC_removeItems) then {_unit removeMagazineGlobal _x}} forEach magazines _unit;
		{if ((random 100) > AGC_removeItems) then {_unit removeItem _x}} forEach items _unit;
	},
	_this
] call CBA_fnc_execNextFrame;
true
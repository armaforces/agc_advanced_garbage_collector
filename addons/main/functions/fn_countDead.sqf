 /*
	AGC_fnc_countDead

	Description:
		Counts all dead bodies for each side.

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		Successful <BOOL>
 */

params ["_unit"];
if !(_unit isKindOf "man") exitWith {};
if (_unit getVariable ["AGC_isBodyAgent",false]) exitWith {};
if (isPlayer _unit) then
{
	AGC_deadPlayers = AGC_deadPlayers + 1;
};
private _group = group _unit;
private _side = side _group;
if (_side == east) exitWith {
	AGC_deadEAST = AGC_deadEAST + 1;
};
if (_side == west) exitWith {
	AGC_deadWEST = AGC_deadWEST + 1;
};
if (_side == resistance) exitWith {
	AGC_deadGUER = AGC_deadGUER + 1;
};
if (_side == civilian) exitWith {
	AGC_deadCIV = AGC_deadCIV + 1;
};
AGC_deadUnknown = AGC_deadUnknown + 1;
true

 /*
	AGC_fnc_hideBody

	Description:
		Hides body when max time and distance is reached.

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		Successful <BOOL>
 */

params ["_unit","_findFarPlayer"];
private _hidden = isObjectHidden _unit;
if (_hidden) then
{
	if (_findFarPlayer != -1) then
	{
		_unit hideObjectGlobal false;
	};
}else
{
	if (_findFarPlayer == -1) then
	{
		_unit hideObjectGlobal true;
	};
};
true

 /*
	AGC_fnc_GCloop

	Description:
		Init garbage collector. Server only.

	Arguments:
		None (setup in CBA settings)
	
	Return Value:
		Successful <BOOL>
 */

if (!isServer) exitWith {false};
if !(isNil "AGC_GCloopHandler") then
{
	[AGC_GCloopHandler] call CBA_fnc_removePerFrameHandler;
};
if (AGC_GCenabled) then
{
	AGC_GCloopHandler =
	[
		{
			if !(AGC_GCdeadBodies isEqualTo []) then
			{
				private _checkTimes = ceil (count AGC_GCdeadBodies / AGC_checkTimes);
				for "_i" from 0 to _checkTimes do
				{
					call AGC_fnc_checkBody;
				};
			};
		},
		AGC_GCrefreshInterval
	] call CBA_fnc_addPerFrameHandler;
}else
{
	{
		if (!(_x isKindOf "Man") && !(_x isEqualTo objNull)) then
		{
			_x call AGC_fnc_bodyCacheLoad;
		};
	}forEach AGC_GCdeadBodies;
};
true

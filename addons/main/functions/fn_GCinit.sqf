 /*
	AGC_fnc_GCinit

	Description:
		Init AGC on mission start.

	Arguments:
		None

	Return Value:
		None
 */

if !(isServer) exitWith {false};

AGC_deadPlayers = 0;
AGC_deadWEST = 0;
AGC_deadEAST = 0;
AGC_deadGUER = 0;
AGC_deadCIV = 0;
AGC_deadUnknown = 0;
AGC_deadVehicles = 0;

addMissionEventHandler ["EntityKilled",
{
	params [["_unit",objNull,[objNull]]];
	if (_unit isEqualTo objNull) exitWith {};
	if (_unit isKindOf "man") then
	{
		if (!isPlayer _unit) then
		{
			_unit setVariable ["AGC_GCdeathTime",time];
			if (_unit getVariable ["AGC_removeItems",true]) then
			{
				[_unit] remoteExecCall ["AGC_fnc_removeItems",_unit];
			};
			if !(_unit getVariable ["GCblackList", false]) then
			{
				AGC_GCdeadBodies pushBack _unit;
				_unit setVariable ["GCblackList", true];
				[_unit] call AGC_fnc_getWeaponHolder;
			};
		};
		[_unit] call AGC_fnc_countDead;
	}else
	{
		if (_unit isKindOf "AllVehicles") then
		{
			if (AGC_GCvehicleCleanupDistance > 0) then
			{
				[{
					_unit = _this select 0;
					_garbagecollector = nearestObjects [_unit, ["AllVehicles"], AGC_GCvehicleCleanupDistance];
					{
						if (!alive _x) then
						{
							{deleteVehicle _x}forEach crew _x;
						}
					} forEach _garbagecollector;
				}, [_unit], 1] call CBA_fnc_waitAndExecute;
			};
		};
	};
}];

AGC_GCdeadBodies = [];
call AGC_GCloop;
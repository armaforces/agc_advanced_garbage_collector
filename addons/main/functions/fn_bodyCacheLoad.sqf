 /*
	AGC_fnc_bodyCacheLoad
	
	Description:
		Replaces simple object with dead body on player presence.

	Arguments:
		0: Simple object <OBJECT>

	Return Value:
		Dead agent body <OBJECT>
 */

params ["_body"];
private _pos = getposATL _body;
private _dir = getDir _body;
private _Identity = _body getVariable ["AGC_Identity",["",""]];
_Identity params ["_face","_name"];
(_body getVariable "AGC_cacheLoadout") params ["_type","_loadout"]; 
private _weaponsArray = _body getVariable ["AGC_GCWeaponHolder",[]];
deleteVehicle _body;
private _agent = createAgent [_type, [0,0,0], [], 0, "CAN_COLLIDE"];
_agent setUnitLoadout _loadout;
if (_face != "") then {_agent setFace _face};
if (_name != "") then {_agent setName _name};
{
	{
		_x params ["_weapon","_item1","_item2","_item3","_mag1","_item4"];
		_agent addWeapon _weapon;
		_agent addWeaponItem [_weapon,_item1];
		_agent addWeaponItem [_weapon,_item2];
		_agent addWeaponItem [_weapon,_item3];
		_agent addWeaponItem [_weapon,_item4];
		if !(_mag1 isEqualTo [])then
		{
			_agent addWeaponItem [_weapon,_mag1];
		};
	}forEach _x;
}forEach _weaponsArray;
_agent setVariable ["AGC_removeItems",false];
_agent setDir _dir;
private _anim = "deadstate";
if (AGC_ragdoll) then
{
	_anim = selectRandom [
	"AmovPincMstpSrasWrflDnon",
	"AadjPpneMstpSrasWpstDdown",
	"AinvPpneMstpSrasWrflDnon_Putdown",
	"AinvPpneMstpSrasWrflDnon",
	"Hepler_InjuredNon"
	];
};
[_agent, _anim] remoteExecCall ["switchMove",0,false];
_agent setVariable ["GCblackList", true];
_agent setVariable ["AGC_isBodyAgent", true];
_agent setDammage 1;
[{
	[{
		[{
			params ["_agent","_pos"];
			_agent setPos _pos;
		}, _this] call CBA_fnc_execNextFrame;
	}, _this] call CBA_fnc_execNextFrame;
}, [_agent,_pos]] call CBA_fnc_execNextFrame;
[_agent] call AGC_fnc_getWeaponHolder;
{_x addCuratorEditableObjects [[_agent],false]}forEach allCurators;
_agent

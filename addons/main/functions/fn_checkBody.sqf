 /*
	AGC_fnc_checkBody

	Description:
		check Body for cache;

	Arguments:
		None
	
	Return Value:
		Successful <BOOL>
 */

	private _unit = AGC_GCdeadBodies deleteAt 0;
	if (_unit isEqualTo objNull) exitWith {};
	private _deathTime = _unit getVariable ["AGC_GCdeathTime",0];
	if ((_deathTime + AGC_GCminTime) < time && {isNull objectParent _unit})then
	{
		[_unit] call AGC_fnc_GChandler;
	}else
	{
		AGC_GCdeadBodies pushBack _unit;
	};
true

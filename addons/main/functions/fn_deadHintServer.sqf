 /*
	AGC_fnc_deadHintServer

	Description:
		Send to clients info about how many casualties been during mission.

	Arguments:
		0: show info to all players <BOOL>

	Return Value:
		Successful <BOOL>
 */

params [["_showToAll",false]];
_Text = format [
"Dead West = %1 \nDead East = %2 \nDead Resistance = %3 \nDead Civilians = %4 \nDead Unknown = %5 \n\nDead Players = %6 \n\nTotal casualties = %7",
AGC_deadWEST,
AGC_deadEAST,
AGC_deadGUER,
AGC_deadCIV,
AGC_deadUnknown,
AGC_deadPlayers,
AGC_deadWEST + AGC_deadEAST + AGC_deadGUER + AGC_deadCIV + AGC_deadUnknown
];
if (_showToAll) then
{
	[_Text] remoteExecCall ["hintSilent",0,false];
}else
{
	[_Text] remoteExecCall ["hintSilent",remoteExecutedOwner,false];
};
true

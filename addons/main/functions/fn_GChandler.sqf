 /*
	AGC_fnc_GChandler

	Description:
		handle what to do with dead body/cached body (on every script tick).

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		Successful <BOOL>
 */

params ["_unit"];
private _list = _unit nearEntities ["Man", AGC_GCminDistance];
private _findPlayer = _list findIf {isPlayer _x && {alive _x}};
if (_unit isKindOf "Man") then
{
	if (_findPlayer == -1) then
	{
		//replace body with simple object if no nearby players present
		private _body = [_unit] call AGC_fnc_bodyCache;
		AGC_GCdeadBodies pushBack _body;
	}else
	{
		if (AGC_GCmaxBodyTime > 0) then
		{
			if (time > ((_unit getVariable ["AGC_GCdeathTime",time]) + AGC_GCmaxBodyTime))then
			{
				[_unit] remoteExecCall ["hideBody",_unit,false];
			}else
			{
				AGC_GCdeadBodies pushBack _unit;
			};
		}else
		{
			AGC_GCdeadBodies pushBack _unit;
		};
	};
}else
{
	if (_findPlayer != -1) then
	{
		//replace simple object with body if nearby players present
		private _body = [_unit] call AGC_fnc_bodyCacheLoad;
		AGC_GCdeadBodies pushBack _body;
	}else
	{
		if (AGC_GCmaxTime > 0) then
		{
			if (time > ((_unit getVariable ["AGC_bodyTime",0]) + AGC_GCmaxTime))then
			{
				private _farList = _unit nearEntities ["Man", AGC_GCmaxDistance];
				private _findFarPlayer = _farList findIf {isPlayer _x && {alive _x}};
				if (AGC_maxBodyHandle isEqualTo 0)exitWith
				{
					if (_findFarPlayer == -1) then
					{
						deleteVehicle _unit;
					}else
					{
						AGC_GCdeadBodies pushBack _unit;
					};
				};
				if (AGC_maxBodyHandle isEqualTo 1)exitWith
				{
					[_unit,_findFarPlayer] call AGC_fnc_hideBody;
					AGC_GCdeadBodies pushBack _unit;
				};
			}else
			{
				AGC_GCdeadBodies pushBack _unit;
			};
		}else
		{
			AGC_GCdeadBodies pushBack _unit;
		};
	};
};
true

 /*
	AGC_fnc_bodyCache

	Description:
		Replaces dead body with simple object (much more performance efficient than body).

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		Simple object <OBJECT>
 */

params ["_unit"];
private _pos = getposATL _unit;
//private _posWorld = getPosWorld _unit;
private _dir = getDir _unit;
private _type = typeOf _unit;
private _loadout = getUnitLoadout _unit;
private _holder = _unit getVariable ["AGC_GCWeapon",[]];
private _weaponsArray = [];
{
	private _getweap = weaponsItemsCargo _x;
	_weaponsArray pushBack _getweap;
}forEach _holder;
private _face = face _unit;
private _name = name _unit;
deleteVehicle _unit;
//_body = [_type, _posWorld, _dir] call BIS_fnc_createSimpleObject;
private _body = createVehicle [AGC_bodyType, _pos, [], 0, "CAN_COLLIDE"];
_body setVariable ["AGC_bodyTime",time];
_body enableSimulationGlobal false;
_body setDir _dir;
private _normal = surfaceNormal _pos;
_body setVectorUp _normal;
private _bodyPos = getposATL _body;
if ((_bodyPos select 2) < 0) then
{
	_bodyPos set [2,0];
	_body setposATL _bodyPos;
};
_body setVariable ["AGC_cacheLoadout",[_type,_loadout]];
_body setVariable ["AGC_GCWeaponHolder",_weaponsArray];
_body setVariable ["AGC_Identity",[_face,_name]];
{_x addCuratorEditableObjects [[_body],false]}forEach allCurators;
_body

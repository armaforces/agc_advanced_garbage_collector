 /*
	AGC_fnc_getWeaponHolder

	Description:
		Retrive weapon holder from dead body. 0.5s delay, repeat on failed attempt (max 5 times).

	Arguments:
		0: Unit <OBJECT>

	Return Value:
		None (set variable "AGC_GCWeapon" on body with weapon holder )
 */

[{
	private _unit = _this select 0;
	if (_unit == objNull) exitWith {};
	private _holders = nearestObjects [getPos _unit, ["WeaponHolderSimulated", "GroundWeaponHolder", "Default"], 2];
	if !(_holders isEqualTo []) then
	{
		private _unitHolders = [];
		{
			if (_x getVariable ["AGC_weaponOwner",objNull] isEqualTo objNull) then
			{
				_x setVariable ["AGC_weaponOwner",_unit];
				_unitHolders pushBack _x;
			};
		}forEach _holders;
		_unit setVariable ["AGC_GCWeapon",_unitHolders];
		_unit setVariable ["AGC_holderCheckCount",nil];
	}else
	{
		private _counter = _unit getVariable ["AGC_holderCheckCount",0];
		if (_counter < 5) then
		{
			_unit setVariable ["AGC_holderCheckCount",_counter + 1];
			[_unit] call AGC_fnc_getWeaponHolder;
		};
	};
}, _this, 0.5] call CBA_fnc_waitAndExecute;
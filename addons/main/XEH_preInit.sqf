[
	// Global var name
	"AGC_GCenabled",
	// Setting type
	"CHECKBOX",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_gcEnabled1",
		localize "STR_AGC_CBA_gcEnabled2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	true,
	// Is global
	true,
	// Init/Change script
	{call AGC_fnc_GCloop},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	"AGC_bodyType",
	"LIST",
	[
		localize "STR_AGC_CBA_bodyType1",
		localize "STR_AGC_CBA_bodyType2"
	],
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	[
		["Land_Bodybag_01_black_F","body"],
		["Vanilla body bag","ACE body bag"],
		0
	]
] call cba_settings_fnc_init;

[
	// Global var name
	"AGC_GCminTime",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCminTime1",
		localize "STR_AGC_CBA_GCminTime2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[5, 600, 60, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_GCminDistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCminDistance1",
		localize "STR_AGC_CBA_GCminDistance2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[5, 600, 60, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_GCrefreshInterval",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCrefreshInterval1",
		localize "STR_AGC_CBA_GCrefreshInterval2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[0, 0.2, 0.00, 2],
	// Is global
	true,
	// Init/Change script
	{call AGC_fnc_GCloop},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_checkTimes",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_checkTimes1",
		localize "STR_AGC_CBA_checkTimes2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[50, 500, 100, 0],
	// Is global
	true,
	// Init/Change script
	{call AGC_fnc_GCloop},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_GCmaxTime",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCmaxTime1",
		localize "STR_AGC_CBA_GCmaxTime2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[0, 1800, 0, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_GCmaxDistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCmaxDistance1",
		localize "STR_AGC_CBA_GCmaxDistance2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[5, 5000, 1000, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	"AGC_maxBodyHandle",
	"LIST",
	[
		localize "STR_AGC_CBA_maxBodyHandle1",
		localize "STR_AGC_CBA_maxBodyHandle2"
	],
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	[
		[0,1],
		[localize "STR_AGC_CBA_maxBodyHandleDelete",localize "STR_AGC_CBA_maxBodyHandleHide"],
		0
	]
] call cba_settings_fnc_init;

[
	// Global var name
	"AGC_GCmaxBodyTime",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCmaxBodyTime1",
		localize "STR_AGC_CBA_GCmaxBodyTime2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	[0, 1800, 0, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_ragdoll",
	// Setting type
	"CHECKBOX",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_ragdoll1",
		localize "STR_AGC_CBA_ragdoll2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GC"],
	// Extra params, depending on settings type
	true,
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_GCvehicleCleanupDistance",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_GCvehicleCleanupDistance1",
		localize "STR_AGC_CBA_GCvehicleCleanupDistance2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GCextras"],
	// Extra params, depending on settings type
	[0, 20, 10, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_removeItems",
	// Setting type
	"SLIDER",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_removeItems1",
		localize "STR_AGC_CBA_removeItems2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GCextras"],
	// Extra params, depending on settings type
	[0, 100, 100, 0],
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

[
	// Global var name
	"AGC_showBodyCount",
	// Setting type
	"CHECKBOX",
	// Title, Tooltip
	[
		localize "STR_AGC_CBA_showBodyCount1",
		localize "STR_AGC_CBA_showBodyCount2"
	],
	// Category, Subcategory
	["Advanced Garbage Collector", localize "STR_AGC_CBA_category_GCextras"],
	// Extra params, depending on settings type
	false,
	// Is global
	true,
	// Init/Change script
	{},
	// Needs mission restart
	false
] call CBA_Settings_fnc_init;

["Advanced Garbage Collector","AGC_showBodyCount_Key", localize "STR_AGC_CBA_showBody", {[false] call AGC_fnc_deadHint}, ""] call CBA_fnc_addKeybind;
["Advanced Garbage Collector","AGC_showBodyCountAll_Key", localize "STR_AGC_CBA_showBodyAll", {[true] call AGC_fnc_deadHint}, ""] call CBA_fnc_addKeybind;


diag_log text "[AGC] PreInit";

class CfgFunctions
{
	class AGC {
		
		class functions {

			file = "AGC\framework\main\functions";
		
			class bodyCache;

			class bodyCacheLoad;	

			class hideBody;

			class GChandler;

			class GCinit						{ preInit = 1};

			class GCloop;

			class getWeaponHolder;	
			
			class removeItems;

			class deadHint;

			class deadHintServer;

			class countDead;
			
			class checkBody
			
		};		
	};
};